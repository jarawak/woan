# WOAN

Web Open Audio Navigation Specification

WOAN Spec is an initiative from team Jarawak to turn web content more accessible for visual disabled people and create a new way to navigate in web content without the use of a visual interface.

### Checkout related open-source projects ###

[Jarawak Open Audio Navigation Library for Java and Android JOANlib](https://bitbucket.org/jarawak/openaudionav)

[Jarawak Open Audion Navigation Builder JavaFX](https://bitbucket.org/jarawak/opennavbuilder)

[Jarawak Android OAN Browser](https://bitbucket.org/jarawak/joan_android_browser)

[Jarawak Library for Java and Android](https://bitbucket.org/jarawak/jarawak)

Jarawak Project intent to provide open-source tools to build web/java/mobile applications. Include REST Webservice client, Servlets for REST API building and examples, SmartModel for MVC development and database persistence, convertions tools and more.

### License ###

All this Work is under Apache 2.0 license, initially attributed to Leonardo Germano Roese (author of initiative and initial developments) 
	
#### Copyright 2018 Leonardo Germano Roese ####
	 
Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at
	 
[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
	 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
	 
